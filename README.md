# Docker CentOS with superpowers!

## Overview

- `dmglab/centos:latest` Centos 7 image with EPEL + Some Utils
- `dmglab/centos:7epel` epel enabled Centos 7 image (base for latest)
- `dmglab/centos:6` Centos 6 image with EPEL + Some Utils
- `dmglab/centos:6epel` epel enabled Centos 6 image

## Utils list (WIP)

All Utils are installed via yum and available via `$DOCKER_UTILS` if you want to remove them

- vim
- telnet
- net-tools
- lsof
- pwgen